package datepicker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestCase {
	

	public static void main(String[] args) throws IOException {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(System.getProperty("user.dir") + "\\config.properties");

		// load a properties file
		prop.load(input);
		String url = prop.getProperty("url");
		// Launch website
		driver.navigate().to(url);
		driver.manage().window().maximize();

		File file = new File("C:\\Users\\Indium Software\\Downloads\\data.xlsx");
		FileInputStream inputStream = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wb.getSheet("Datadriven");
		TestCase t= new TestCase ();
		String tescaseName = t.getClass().getSimpleName();
		
		System.out.println("Testcase name is" + tescaseName);

		for (int i = 1; i <= sheet.getLastRowNum(); i++) {

			Row row = sheet.getRow(i);

			// Create a loop to print cell values in a row
			
			for (int j = 0; j < row.getLastCellNum(); j++) {

				// Print Excel data in console
				Cell cell = row.getCell(j);
				String userName = null;
				String password = null;
				String name = row.getCell(0).getStringCellValue().trim();
				String executionFlag = row.getCell(1).getStringCellValue().trim();
				if (name.equalsIgnoreCase(tescaseName)) {
					if (executionFlag.equalsIgnoreCase("yes")) {
						if (j == (row.getLastCellNum()-2)) {
							userName = cell.getStringCellValue();
							WebElement user = driver.findElement(By.cssSelector("#txtUsername"));
							user.clear();
							user.sendKeys(userName);
						}
						if (j == (row.getLastCellNum()-1)) {
							password = cell.getStringCellValue();
							WebElement passwordField = driver.findElement(By.cssSelector("#txtPassword"));
							passwordField.clear();
							passwordField.sendKeys(password);
							driver.findElement(By.cssSelector("#btnLogin")).click();
						}

					}
				}
			}

		}

	}
}
