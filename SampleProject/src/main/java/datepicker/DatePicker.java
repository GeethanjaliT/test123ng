package datepicker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DatePicker {
	public static void main(String[] args) throws IOException {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(System.getProperty("user.dir") + "\\config.properties");

		// load a properties file
		prop.load(input);
		String url = prop.getProperty("url");
		String year = prop.getProperty("year");
		String month = prop.getProperty("month").trim();
		String date = prop.getProperty("date").trim();
		// Launch website
		driver.navigate().to(url);
		driver.manage().window().maximize();
		driver.findElement(By.cssSelector("#txtUsername")).sendKeys("Admin");
		driver.findElement(By.cssSelector("#txtPassword")).sendKeys("admin123");
		driver.findElement(By.cssSelector("#btnLogin")).click();
		driver.findElement(By.cssSelector("#menu_admin_viewAdminModule")).click();
		driver.findElement(By.cssSelector("#menu_pim_viewMyDetails")).click();
		driver.findElement(By.cssSelector("#btnSave")).click();
	//	driver.findElement(By.cssSelector("#personal_txtLicExpDate")).click();
		driver.findElement(By.cssSelector("#personal_DOB")).click();
	
	String actualYear=driver.findElement(By.xpath("//select[@class='ui-datepicker-month']//option[@selected='selected']")).getText().trim();
	
	
	while(!actualYear.equalsIgnoreCase(month))
	{
		driver.findElement(By.xpath("//a[@data-handler='next']")).click();
		actualYear=driver.findElement(By.xpath("//select[@class='ui-datepicker-month']//option[@selected='selected']")).getText().trim();
	}
	
	driver.findElement(By.cssSelector(".ui-datepicker-year")).click();
	Select years= new Select(driver.findElement(By.cssSelector(".ui-datepicker-year")));
	
	years.selectByVisibleText(year);
	driver.findElement(By.xpath("//a[text()='"
			+ date
			+ "']")).click();
	
	}
}
