package datepicker;

import java.io.IOException;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class download {
	public static void main(String[] args) throws IOException {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));
		WebDriver driver = new ChromeDriver(capabilities);
	//	WebDriver driver = new ChromeDriver();
		driver.get("https://file-examples.com/index.php/sample-documents-download/");
	    WebElement ele=	driver.findElement(By.xpath("//table[@id='table-files']//tr/td[contains(text(),'PDF')]/following-sibling::td[2]"));
	
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
		ele.click();
		//driver.switchTo().alert().dismiss();
		WebElement v=driver.findElement(By.xpath("//iframe[@name='aswift_1']"));
		driver.switchTo().frame(v);
		driver.findElement(By.xpath("//span[text()='Close']")).click();
		driver.switchTo().defaultContent();
	
		//driver.quit();
		
}
}