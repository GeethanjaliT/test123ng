package datepicker;

import org.junit.runner.RunWith;

import cucumber.junit.Cucumber;

@RunWith(Cucumber.class) 
@Cucumber.Options(features = "Feature"
,glue={"StepDefinition"},
monochrome=true
,format = {"pretty", "html:target/cucumber"}) 
public class TestRunner {

}
