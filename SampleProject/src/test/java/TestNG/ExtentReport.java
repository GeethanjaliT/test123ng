package TestNG;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import junit.framework.Assert;
import resource.base;

public class ExtentReport extends base{
	public WebDriver driver;
	ExtentReports extent;
@BeforeTest
public void config()
{
	String path =System.getProperty("user.dir")+"\\reports\\index.html";
	ExtentSparkReporter reporter = new ExtentSparkReporter(path);
	reporter.config().setReportName("Web Automation Results");
	reporter.config().setDocumentTitle("Test Results");
	
	 extent =new ExtentReports();
	extent.attachReporter(reporter);
	extent.setSystemInfo("Tester", "Anjali");
	
	
}
@Test
public void initialExtentReport() throws IOException{
	extent.createTest("initialExtentReport");
	driver=IntializeDriver();
	//driver.findElement(By.xpath("//input[@id='vv']")).click();
	extent.flush();
	
}
/*public static void main(String[] args) throws IOException {
	ExtentReport e= new ExtentReport();
	e.config();
	e.initialExtentReport();
	
}*/
}
