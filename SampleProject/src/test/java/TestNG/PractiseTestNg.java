package TestNG;

import org.testng.ITestContext;
import org.testng.annotations.Test;

public class PractiseTestNg {
	@Test(priority =1)
	public void SwitchON(ITestContext context)
	{
	System.out.println("Laptop is switched ON");
	context.setAttribute("LaptopName", "DELL");
	}



	@Test(priority =1, dependsOnMethods = "SwitchON")
	public void Operating(ITestContext context)
	{
	System.out.println("Laptop is Running");
	String laptopName = (String)context.getAttribute("LaptopName");
	System.out.println("The Name of the Brand:"+laptopName);
	context.setAttribute("LaptopSpeed", "Very Fast");
	}



	@Test(priority =1, dependsOnMethods = {"SwitchON", "Operating"})
	public void SwitchOFF(ITestContext context)
	{
	System.out.println("A Laptop is switched OFF");
	String laptopSpeed = (String)context.getAttribute("LaptopSpeed");
	System.out.println(laptopSpeed);
	}
}
