package TestNG;

import org.testng.ITestContext;
import org.testng.annotations.Test;

public class ITextContextAndDependencyAndSkip {

		@Test(priority =1)
		public void generateData(ITestContext context)
		{
			String firstName = "Amod";
			// Setting an attribute with name and its value
			context.setAttribute("FirstName", firstName);
		}
		
		
		@Test(priority =1, dependsOnMethods = "generateData")
		public void useData(ITestContext context)
		{
			String lastName = "Mahajan";
			context.setAttribute("LastName", lastName);
			// Retrieving attribute value set in ITestContext
			String FN = (String) context.getAttribute("FirstName");
			String fullName = FN +" "+lastName;
			System.out.println("Full Name is : "+fullName);
			context.setAttribute("FullName", fullName);
		}
		@Test(priority =1, dependsOnMethods = {"generateData", "useData"})
		public void useDataInOtherClass(ITestContext context)
		{
			String profession = "Blogger";
			String fullName = (String) context.getAttribute("FullName");
			String nameWithProfession = fullName +" "+profession;
			System.out.println("Full Name is : "+fullName);
			System.out.println("Full Name with profession is "+ nameWithProfession);
		}
	
		@Test(enabled=false)
		public void skip()
		{
			System.out.println("Successfully skipped");
		}
	}

