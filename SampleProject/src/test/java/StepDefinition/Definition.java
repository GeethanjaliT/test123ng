package StepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;

import cucumber.annotation.After;
import cucumber.annotation.en.And;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;
import resource.base;


public class Definition extends base {
	public WebDriver driver;


	@Given("^user navigates to the website orangecrm")
	public void user_navigates_to_the_website_75healthcom() throws Throwable {
		driver=IntializeDriver();

	}

	@And("^user login to application using Username as \"([^\"]*)\" and Password as \"([^\"]*)\"$")
	public void user_login_to_application_using_username_as_something_and_password_as_something(String strArg1,
			String strArg2) throws Throwable {
		driver.findElement(By.cssSelector("#txtUsername")).sendKeys(strArg1);
		driver.findElement(By.cssSelector("#txtPassword")).sendKeys(strArg2);
		
	}
	@When("^user click on signin$")
	public void user_click_on_signin() throws Throwable {
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("#btnLogin")).click();

	}
	@Then("^homepage is populated$")
	public void homepage_is_populated() throws Throwable {
		
	WebElement homePage=driver.findElement(By.xpath("//img[@alt='OrangeHRM']"));
	if(homePage.isDisplayed())
	{
		System.out.println("homepage got displayed");
	}
	}

	@After()
	public void teardown() {
		driver.quit();
	}

}
