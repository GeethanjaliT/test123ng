package com.reflection;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Reflection {
	public static void main(String[] args) throws Exception
    {
        doRegular();
       doReflection();
    }

    public static void doRegular() throws Exception
    {
        long start = System.currentTimeMillis();
        for (int i=0; i<100; i++)
        {
            A a = new A();
            a.doSomeThing();
        }
        System.out.println(System.currentTimeMillis() - start);
    }

    public static void doReflection() throws Exception
    {
        long start = System.currentTimeMillis();
        for (int i=0; i<100; i++)
        {
        	/*A a = (A) Class.forName("misc.A").newInstance();
            a.doSomeThing();*/
        	Class obj_test = A.class;
        	 Method[] methods = obj_test.getDeclaredMethods(); 
        	 for(Method m : methods) {               
                 System.out.println("Method Name: " + m.getName());
                 int modifier = m.getModifiers();
                 System.out.print("Modifier: " + Modifier.toString(modifier) + "  ");
                 
                 // get the return type of method
                 System.out.print("Return Type: " + m.getReturnType());
                 System.out.println("\n");
                
   
        }
        System.out.println(System.currentTimeMillis() - start);
    }
}}
