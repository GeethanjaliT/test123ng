package com.reflection;

public class WaysToCreateObject {
	private void method1()
	{
		
	}
	public void method2()
	{
		
	}
	public static void main (String[] args) throws ClassNotFoundException {
		//1 - By using Class.forname() method 
	//	Class c1 = Class.forName("WaysToCreateObject"); 
		
		//2- By using getClass() method 
		WaysToCreateObject demo = new WaysToCreateObject();
		Class c2 = demo.getClass();
		//3- By using .class 
		Class c3= WaysToCreateObject.class;
		System.out.println("Name of the class is"+ c3.getName());
		System.out.println("Name of the super class is : " +c3.getSuperclass().getName());
		}
	}

